---
title: APT-LISTBUGS
header: General Commands Manual
date: November 2024
section: 1
---


# NAME

apt-listbugs - Lists critical bugs before each APT installation/upgrade

# SYNOPSIS

**apt-listbugs** \[**-s** *severities*\] \[**-T** *tags*\] \[**-S** *states*\]
\[**-B** *bug#*\] \[**-r** *release*\] \[**-D**\] \[**-u** *url*\]
\[**-H** *hostname*\] \[**-p** *port*\] \[**-P** *priority*\]
\[**-E** *title*\] \[**-q**\] \[**-C** *apt.conf*\] \[**-F**\] \[**-N**\]
\[**-y**\] \[**-n**\] \[**-a**\] \[**-d**\] *command* \[*argument* \...\]

**apt-listbugs -h**

**apt-listbugs -v**

# DESCRIPTION

**apt-listbugs** is a tool which retrieves bug reports from the Debian
Bug Tracking System and lists them. In particular, it is intended to be
invoked before each installation or upgrade by APT, or other compatible
package managers, in order to check whether the installation/upgrade is
safe.

In the typical use case, the user is installing or upgrading a number of
packages with APT or some other compatible package manager. Before the
package installation or upgrade is actually performed, **apt-listbugs**
is automatically invoked: it queries the Debian Bug Tracking System for
bugs (of certain configured severities) that would be introduced into
the system by the installation or upgrade; if any such bug is found,
**apt-listbugs** warns the user and asks how to proceed. Among other
things, the user has the opportunity to continue, to abort the
installation or upgrade, or to pin some packages (so that the unsafe
installation or upgrade is deferred). However, pinning is not effective
immediately, and requires restarting the APT session (by aborting and
then re-running the same APT command).

Each package pin is automatically removed by a daily cron job (or by an
equivalent systemd timer), as soon as the corresponding bug is fixed in
(or no longer affects) the package version available for installation or
upgrade. When the pin is removed, the installation or upgrade of the
package becomes possible again.

In order for the automatic pin removal to work correctly, the daily cron
job or systemd timer has to be actually executed: if systemd is used as
init system, this should be taken care of automatically; otherwise, if
the system is up and running almost 24/7, then **cron** should suffice;
in all other cases, the installation of **anacron** is recommended.
Moreover the Internet link must be working, while the daily job is run.
Finally, the APT package lists should be kept up-to-date.

# OPTIONS

**-s** *severities* **, \--severity** *severities*

:   Filter (and sort) bugs by severity, showing only the bugs matching
specified values. List the bug severities that you want to see, separated by
commas and in the desired order. Possible values are **critical**, **grave**,
**serious**, **important**, **normal**, **minor**, **wishlist**, or the special
value **all** to disable filtering. Default: **critical,grave,serious**. The
default list may be changed by setting the **AptListbugs::Severities**
configuration option.

**-T** *tags* **, \--tag** *tags*

:   Filter bugs by tags, showing only the bugs matching all specified values.
List the tags that you want to see, separated by commas. Default: no filter.
Possible values include **confirmed,l10n** to show only bugs that have both
these tags.

**-S** *states* **, \--stats** *states*

:   Filter (and sort) bugs by pending-state, showing only the bugs matching
specified values. List the pending-state categories that you want to see,
separated by commas and in the desired order. Default:
**pending,forwarded,pending-fixed,fixed,done**. Possible values are:
**pending** (open bug), **forwarded** (marked as \"forwarded\"),
**pending-fixed** (tagged as \"pending\"), **fixed** (tagged as \"fixed\"),
**absent** (not found in this distribution/architecture), **done** (resolved in
some version for this distribution/architecture).  Note that a bug can only
match one such state (when multiple conditions on this list match, the later
one takes priority), and that **pending** does not mean \"tagged as pending\".

**-B** *bug#* **, \--bugs** *bug#*

:   Filter bugs by number, showing only the bugs directly specified. List the
bug numbers that you want to see, separated by commas (e.g.
**123456,567890,135792**). Default: no filter.

**-r** *release* **, \--distro-release** *release*

:   Filter bugs by distribution release, showing only the bugs that affect the
specified release. Possible values are one of the release codenames listed in
the output of the command \"**distro-info** \--codename \--all\", or
**unstable**, **testing**, **stable**, **oldstable**, or the special value
**ANY** to disable filtering. Default: no filter. The default value may be
changed by setting the **AptListbugs::DistroRelease** configuration option.

**-D, \--show-downgrade**

:   Show bugs of downgraded packages. (apt mode only)

**-u** *url* **, \--url** *url*

:   Specifies the SOAP URL for the Debian Bug Tracking System. Default:
**https://bugs.debian.org:443/cgi-bin/soap.cgi** . The default URL may be
changed by setting the **AptListbugs::URL** configuration option.

**-H** *hostname* **, \--hostname** *hostname*

:   Specifies the hostname of the Debian Bug Tracking System and causes http to
be used instead of https (DEPRECATED: use **-u** instead). Default:
**bugs.debian.org** .

**-p** *port* **, \--port** *port*

:   Specifies the port number of the SOAP interface of the Debian Bug Tracking
System and causes http to be used instead of https (DEPRECATED: use **-u**
instead). Default: 80.

**-P** *priority* **, \--pin-priority** *priority*

:   Specifies **Pin-Priority** value. Default: 30000.

**-E** *title* **, \--title** *title*

:   Specifies the title of RSS output.

**-q, \--quiet**

:   Don't display progress bar. This option is assumed if stdout is not a
terminal.

**-C** *apt.conf* **, \--aptconf** *apt.conf*

:   Specifies an additional APT configuration file to use. This file will be
read after the default APT configuration files.

**-F, \--force-pin**

:   When in apt mode, assumes that you want to automatically pin all buggy
packages without any prompt. This option is assumed if stdout is not a
terminal, unless the **-N** command-line option is used.

**-N, \--force-no-pin**

:   When in apt mode, never automatically pin any package without prompt. This
is the default behavior, as long as stdout is a terminal.

**-y, \--force-yes**

:   Assumes that you select yes for all questions. When in apt mode, this
implies that you accept to continue with the installation/upgrade, even when
bugs are found or errors occur.

**-n, \--force-no**

:   Assumes that you select no for all questions. When in apt mode, this
implies that you want to abort the installation/upgrade, as soon as bugs are
found or errors occur. This option is assumed if stdout is not a terminal,
unless the **-y** command-line option is used.

**-a, \--force-default**

:   Assumes that you select the default (automatic) reply for all questions.
When in apt mode, this implies that you accept to continue with the
installation/upgrade, even when bugs are found, but not when errors occur.

**-d, \--debug**

:   Give extra debug output, important for debugging problems. Please include
**-d** when reporting problems.

**-h, \--help**

:   Print usage help and exit.

**-v, \--version**

:   Print version number and exit.

# COMMANDS

**apt**

:   Reads package actions from a file descriptor specified in the
\$**APT_HOOK_INFO_FD** environment variable (typically provided by APT or other
compatible package manager; **Pre-Install-Pkgs** hook info protocol version 3
is expected - see **apt.conf**(5) for more details).

**list \[** *package* **\[:** *arch* **\]\[/** *version* **\] \...\]**

:   Reads package names from the arguments and simply lists bugs of these
packages.  Package versions may be specified with a slash, as in **apt/1.0**
for example. Package architectures may be specified with a colon, as in
**apt:amd64** or **apt:amd64/1.0** (but please note that the Debian Bug
Tracking System does not distinguish the architectures, hence the same bugs
will be listed, regardless of the specified architecture).

**rss \[** *package* **\[:** *arch* **\]\[/** *version* **\] \...\]**

:   Reads package names from the arguments and lists bugs of these packages in
RSS format.  Again, package versions may be specified with a slash and
architectures with a colon.

# ENVIRONMENT VARIABLES

**APT_LISTBUGS_FRONTEND**

:   If this variable is set to \"none\", **apt-listbugs** will not execute at
all; this might be useful if you would like to script the use of a program that
calls **apt-listbugs**.

**http_proxy**

:   If \$**http_proxy** is set, the value is used for HTTP Proxy, unless proxy
settings are found in APT configuration (see below).

**APT_HOOK_INFO_FD**

:   File descriptor from which package actions will be read (APT or other
compatible package managers are expected to write information to this file
descriptor and to properly set this environment variable).

# CONFIGURATION FILE

**apt-listbugs** reads the APT configuration (see **apt.conf**(5) for more
details). The following configuration options are recognized:

**Acquire::http::Proxy**

:   Default HTTP Proxy setting (overrides any \$**http_proxy** environment
    variable value). An empty string or the special keyword \"DIRECT\" will
    disable proxy.

**Acquire::http::Proxy-Auto-Detect**

:   Automatic HTTP Proxy discovery (overrides the default HTTP Proxy setting
    and any \$**http_proxy** environment variable value). It can be used to
    specify an external command that is expected to output the proxy on stdout.

**Acquire::http::Proxy::bugs.debian.org**

:   Specific HTTP Proxy setting (overrides any other proxy setting). Useful for
    setting HTTP proxy for **apt-listbugs**. The special keyword \"DIRECT\"
    will disable proxy.

**AptListbugs::URL**

:   Default SOAP URL for the Debian Bug Tracking System. When this option is
    not set, the SOAP URL is \"https://bugs.debian.org:443/cgi-bin/soap.cgi\",
    unless explicitly altered by using the **-u** command-line option (or the
    deprecated **-H** and **-p** command-line options). On the other hand, when
    this option is set, the SOAP URL is its value, unless explicitly altered by
    using the command-line options.

**AptListbugs::Severities**

:   Default (comma-separated) list of bug severities to be shown. When this
    option is not set, the list is \"critical,grave,serious\", unless
    explicitly altered by using the **-s** command-line option. On the other
    hand, when this option is set, the list of severities is its value, unless
    explicitly altered by using the **-s** command-line option.

**AptListbugs::DistroRelease**

:   Default distribution release to consider for affected bugs to be shown.
    When this option is not set or set to \"ANY\", no filtering by release is
    performed, unless explicitly enabled by using the **-r** command-line
    option. On the other hand, when this option is set, the distribution
    release is its value, unless explicitly altered by using the **-r**
    command-line option.

**AptListbugs::IgnoreRegexp**

:   Bugs to ignore when in apt mode. This is evaluated using Ruby regular
    expressions: if the bug title matches, the bug is ignored. Default:
    nothing. A possible suggested value is \"FTBFS\", since those bugs tend to
    not affect the user.

**AptListbugs::QueryStep**

:   Maximum number of packages to be queried (on the Debian Bug Tracking
    System) in a single batch. Default value is 200.  The query operation is
    performed in batches of at most **QueryStep** packages, for performance
    reasons; setting a lower value may slow down **apt-listbugs**, but may
    increase reliability on poor network links.

**AptListbugs::ParseStep**

:   Maximum number of bug reports to be queried (on the Debian Bug Tracking
    System) and parsed in a single batch.  Default value is 200. The query and
    parse operation is performed in batches of at most **ParseStep** bugs, for
    performance reasons; setting a lower value may slow down **apt-listbugs**,
    but may increase reliability on poor network links.

**AptListbugs::DebugFile**

:   File for the debug output. Default value is stderr. When set, the specified
    file will be overwritten with the debug output, if the **-d** option has
    been passed.

**AptListbugs::TimerOutputMail**

:   Boolean option for the systemd timer output. When set to \"false\", the
    output of the systemd timer will not be sent to root via local mail, but to
    the logs. When this option is not set (or set to \"true\", which is the
    default value), the output of the systemd timer will be sent to root via
    local mail, as long as a sendmail compatible Mail Transport Agent (MTA) is
    found on the system.

# OUTPUT EXAMPLE

~~~~~~~
[bug severity] bugs of [package] ([current version] -> [package version to be installed]) <[state of bug report]>
 [bN] - [#bug] - [bug title] [(Fixed: fixed version, if it's fixed in a future version)]
Summary:
 [package]([number of] bugs)
~~~~~~~

e.g.:

~~~~~~~
important bugs of apt-listbugs (0.0.47 -> 0.0.49) <Outstanding>
 b1 - #332442 - apt-listbugs: Apt-listbugs doesn't actually download any bug reports
 b2 - #389903 - apt-listbugs: Does not offer to exit if timeout occurs fetching reports
Summary:
 apt-listbugs(2 bugs)
~~~~~~~

# EXIT STATUS

**0**

:   If the program ran successfully and (when in apt mode) you decided to
    continue with the installation/upgrade. Or otherwise, if a SIGUSR1 was
    received (for instance because you issued the command **killall** -USR1
    apt-listbugs).

**1**

:   If an error occurred.

**10**

:   If the program ran successfully in apt mode, but you decided to abort the
    installation/upgrade.

**130**

:   If a SIGINT was received (for instance because you pressed \[Ctrl+C\]).

N.B.: When the program is invoked by APT, any non-zero exit status will
cause the installation/upgrade to be aborted.

# FILES

*/etc/apt/preferences.d/apt-listbugs*

:   Version preferences file fragment for APT managed by **apt-listbugs**: this
is where the package pins are added by the **apt-listbugs** program and removed
by its daily cron job or systemd timer. This file is managed automatically and
there's normally no need to modify it by hand.

*/var/lib/apt-listbugs/ignore_bugs*

:   Automatic list of bug numbers to be ignored by **apt-listbugs**: this is
where the program saves the bug numbers that the user decided to ignore.  This
file is managed automatically and there's normally no need to modify it by
hand.

*/etc/apt/listbugs/ignore_bugs*

:   User list of bug numbers and packages to be ignored by **apt-listbugs**:
this is where the (root) user may manually add bug numbers or package names
that **apt-listbugs** will ignore. This file is only read by **apt-listbugs**,
but never modified: the (root) user has to edit it by hand. The format is: one
bug number or package name per line; lines whose first non-blank character is
'#' are treated as comments and skipped entirely.

*/etc/apt/apt.conf* **and** */etc/apt/apt.conf.d/\**

:   Default APT configuration files (see **apt.conf**(5) for more details).

*/etc/apt/apt.conf.d/10apt-listbugs*

:   Configuration file fragment for APT containing options related to
    **apt-listbugs**: this is the recommended place where the (root) user may
    tweak the behavior of **apt-listbugs**, but usually no customization is
    required.

# AUTHORS

2002 - 2004: **apt-listbugs** was originally written by Masato Taruishi
\<taru@debian.org\>.

2006 - 2008: Junichi Uekawa \<dancer@debian.org\> rewrote it to handle
BTS Versioning features and the SOAP interface. The **\--bugs** option
was added by Francesco Poli in 2008.

2009 - 2010: **apt-listbugs** was maintained by Francesco Poli
\<invernomuto@paranoici.org\> and Ryan Niebur \<ryan@debian.org\>

2011 - 2012: maintenance was carried on by Francesco Poli and Thomas
Mueller \<thomas.mueller@tmit.eu\>.

2013 - present day: **apt-listbugs** has been maintained by Francesco
Poli.

The latest source code is available from
https://salsa.debian.org/frx-guest/apt-listbugs

# SEE ALSO

**apt.conf**(5), **sensible-browser**(1), **xdg-open**(1),
**www-browser**(1), **querybts**(1)
