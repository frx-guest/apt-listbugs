# Slovak translation of apt-listbugs.
# Copyright (C) 2002-2014 apt-listbugs authors
# This file is distributed under the same license as the apt-listbugs package.
# Ivan Masár <helix84@centrum.sk>, 2009, 2014.
msgid ""
msgstr ""
"Project-Id-Version: apt-listbugs 0.1.16\n"
"Report-Msgid-Bugs-To: invernomuto@paranoici.org\n"
"POT-Creation-Date: 2024-12-02 23:13+0100\n"
"PO-Revision-Date: 2014-09-19 11:50+0200\n"
"Last-Translator: Ivan Masár <helix84@centrum.sk>\n"
"Language-Team: Slovak <debian-l10n-slovak@lists.debian.org>\n"
"Language: sk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"
"X-Generator: Virtaal 0.7.1\n"

#. TRANSLATORS: "E: " is a label for error messages; you may translate it with a suitable abbreviation of the word "error"
#: ../bin/apt-listbugs:56 ../bin/apt-listbugs:89 ../bin/apt-listbugs:94
#: ../bin/apt-listbugs:100 ../bin/apt-listbugs:114 ../bin/apt-listbugs:144
#: ../bin/apt-listbugs:175 ../bin/apt-listbugs:224 ../bin/apt-listbugs:238
#: ../lib/aptlistbugs/logic.rb:319 ../lib/aptlistbugs/logic.rb:323
#: ../lib/aptlistbugs/logic.rb:384 ../lib/aptlistbugs/logic.rb:394
#: ../lib/aptlistbugs/logic.rb:1164 ../lib/aptlistbugs/logic.rb:1176
#: ../lib/aptlistbugs/logic.rb:1189 ../libexec/aptcleanup:52
#: ../libexec/aptcleanup:55
msgid "E: "
msgstr "E: "

#: ../bin/apt-listbugs:57
msgid ""
"This may be caused by a package lacking support for the ruby interpreter in "
"use. Try to fix the situation with the following commands:"
msgstr ""
"To môže byť spôsobené balíkom s chýbajúcou podporou používaného interpretra "
"ruby. Pokúste sa situáciu napraviť nasledujúcimi príkazmi:"

#: ../bin/apt-listbugs:89
msgid ""
"APT_HOOK_INFO_FD is undefined.\n"
msgstr ""
"APT_HOOK_INFO_FD je nedefinované.\n"

#: ../bin/apt-listbugs:94
msgid ""
"APT_HOOK_INFO_FD is not correctly defined.\n"
msgstr ""
"APT_HOOK_INFO_FD nie je správne definované.\n"

#: ../bin/apt-listbugs:100
msgid "Cannot read from file descriptor %d"
msgstr "Nie je možné čítať zo súboru popisovača %d"

#: ../bin/apt-listbugs:114
#, fuzzy
msgid ""
"APT Pre-Install-Pkgs failed to provide the expected 'VERSION 3' string.\n"
msgstr ""
"APT Pre-Install-Pkgs nedodáva očakávaný reťazec „VERSION 3“.\n"

#: ../bin/apt-listbugs:144
#, fuzzy
msgid ""
"APT Pre-Install-Pkgs provided fewer fields than expected.\n"
msgstr ""
"APT Pre-Install-Pkgs dodáva menej polí ako sa očakávalo.\n"

#: ../bin/apt-listbugs:175
#, fuzzy
msgid ""
"APT Pre-Install-Pkgs provided an invalid direction of version change.\n"
msgstr ""
"APT Pre-Install-Pkgs udáva neplatný smer zmeny verzie.\n"

#: ../bin/apt-listbugs:255
msgid "****** Exiting with an error in order to stop the installation. ******"
msgstr "****** Ukončuje sa chybou, aby sa zastavila inštalácia. ******"

#: ../lib/aptlistbugs/logic.rb:50
msgid "Usage: "
msgstr "Použitie: "

#: ../lib/aptlistbugs/logic.rb:51
msgid " [options] <command> [arguments]"
msgstr " [voľby] <príkaz> [argumenty]"

#: ../lib/aptlistbugs/logic.rb:53
msgid ""
"Options:\n"
msgstr ""
"Voľby:\n"

#. TRANSLATORS: the colons (:) in the following strings are vertically aligned, please keep their alignment consistent
#. TRANSLATORS: the \"all\" between quotes should not be translated
#: ../lib/aptlistbugs/logic.rb:56
msgid ""
" -s <severities>  : Filter bugs by severities you want to see\n"
"                    (or \"all\" for all)\n"
"                    [%s].\n"
msgstr ""
" -s <závažnosti>  : Závažnosti, ktoré chcete vidieť\n"
"                    (alebo všetky pomocou „all“)\n"
"                    [%s].\n"

#: ../lib/aptlistbugs/logic.rb:57
msgid ""
" -T <tags>        : Filter bugs by tags you want to see.\n"
msgstr ""
" -T <značky>      : Filtrovať chyby pomocou značiek, ktoré chcete vidieť.\n"

#: ../lib/aptlistbugs/logic.rb:58
msgid ""
" -S <states>      : Filter bugs by pending-state categories you want to see\n"
"                    [%s].\n"
msgstr ""
" -S <stavy>       : Filtrovať chyby pomocou kategórií cieľových stavov,\n"
"                    ktoré chcete vidieť\n"
"                    [%s].\n"

#: ../lib/aptlistbugs/logic.rb:59
msgid ""
" -B <bug#>        : Filter bugs by number, showing only the specified bugs.\n"
msgstr ""
" -B <č. chyby>    : Filtrovať chyby podľa čísla, zobraziť iba určené č. chýb.\n"

#: ../lib/aptlistbugs/logic.rb:60
msgid ""
" -r <release>     : Filter bugs by distribution release, showing only the "
"bugs\n"
"                    that affect the specified release.\n"
msgstr ""

#: ../lib/aptlistbugs/logic.rb:61
msgid ""
" -D               : Show downgraded packages, too.\n"
msgstr ""
" -D               : Zobraziť aj balíky so zníženou verziou.\n"

#: ../lib/aptlistbugs/logic.rb:62
msgid ""
" -u <url>         : SOAP URL for Debian Bug Tracking System\n"
"                    [%s].\n"
msgstr ""

#: ../lib/aptlistbugs/logic.rb:63
msgid ""
" -H <hostname>    : Hostname of Debian Bug Tracking System\n"
"                    (for http, deprecated).\n"
msgstr ""

#: ../lib/aptlistbugs/logic.rb:64
msgid ""
" -p <port>        : Port number of the server\n"
"                    (for http, deprecated).\n"
msgstr ""

#: ../lib/aptlistbugs/logic.rb:65
msgid ""
" -P <priority>    : Pin-Priority value [%s].\n"
msgstr ""
" -P <priority>    : Hodnota Pin-Priority [%s].\n"

#: ../lib/aptlistbugs/logic.rb:66
msgid ""
" -E <title>       : Title of RSS output.\n"
msgstr ""
" -E <titulok>     : Titulok RSS výstupu.\n"

#: ../lib/aptlistbugs/logic.rb:67
msgid ""
" -q               : Don't display progress bar.\n"
msgstr ""
" -q               : Nezobrazovať ukazovateľ priebehu.\n"

#: ../lib/aptlistbugs/logic.rb:68
#, fuzzy
msgid ""
" -C <apt.conf>    : Additional apt.conf file to use.\n"
msgstr ""
" -C <apt.conf>    : Ktorý súbor apt.conf použiť.\n"

#: ../lib/aptlistbugs/logic.rb:69
msgid ""
" -F               : Automatically pin all buggy packages.\n"
msgstr ""
" -F               : Automaticky pripnúť všetky balíky s chybami.\n"

#: ../lib/aptlistbugs/logic.rb:70
#, fuzzy
msgid ""
" -N               : Never automatically pin packages.\n"
msgstr ""
" -F               : Automaticky pripnúť všetky balíky s chybami.\n"

#: ../lib/aptlistbugs/logic.rb:71
#, fuzzy
msgid ""
" -y               : Assume yes for all questions.\n"
msgstr ""
" -y               : Predpokladať odpoveď áno na všetky otázky.\n"

#: ../lib/aptlistbugs/logic.rb:72
#, fuzzy
msgid ""
" -n               : Assume no for all questions.\n"
msgstr ""
" -n               : Predpokladať odpoveď nie na všetky otázky.\n"

#: ../lib/aptlistbugs/logic.rb:73
#, fuzzy
msgid ""
" -a               : Assume the default reply for all questions.\n"
msgstr ""
" -y               : Predpokladať odpoveď áno na všetky otázky.\n"

#: ../lib/aptlistbugs/logic.rb:74
msgid ""
" -d               : Debug.\n"
msgstr ""
" -d               : Ladenie.\n"

#: ../lib/aptlistbugs/logic.rb:75
msgid ""
" -h               : Display this help and exit.\n"
msgstr ""
" -h               : Zobraziť tohto pomocníka a skončiť.\n"

#: ../lib/aptlistbugs/logic.rb:76
msgid ""
" -v               : Show version number and exit.\n"
msgstr ""
" -v               : Zobraziť číslo verzie a skončiť.\n"

#: ../lib/aptlistbugs/logic.rb:77
msgid ""
"Commands:\n"
msgstr ""
"Príkazy:\n"

#: ../lib/aptlistbugs/logic.rb:78
msgid ""
" apt              : Apt mode.\n"
msgstr ""
" apt              : Režim apt.\n"

#: ../lib/aptlistbugs/logic.rb:79
#, fuzzy
msgid ""
" list <pkg>..     : List bug reports of the specified packages.\n"
msgstr ""
" list <bal...>    : Vypísať hlásenia chýb určených balíkov.\n"

#: ../lib/aptlistbugs/logic.rb:80
#, fuzzy
msgid ""
" rss <pkg>..      : List bug reports of the specified packages in RSS.\n"
msgstr ""
" rss <bal...>     : Vypísať hlásenia chýb určených balíkov v RSS.\n"

#: ../lib/aptlistbugs/logic.rb:81
msgid ""
"See the manual page for the long options.\n"
msgstr ""
"Dlhé voľby nájdete v manuálovej stránke.\n"

#. TRANSLATORS: the following six strings refer to a plural quantity of bugs
#. TRANSLATORS: please note that "Outstanding" means "unresolved", not "exceptional"
#: ../lib/aptlistbugs/logic.rb:90
msgid "Outstanding"
msgstr "Nevyriešené"

#: ../lib/aptlistbugs/logic.rb:91
msgid "Forwarded"
msgstr "Preposlané"

#: ../lib/aptlistbugs/logic.rb:92
msgid "Pending Upload"
msgstr "Čakajúce na nahranie"

#: ../lib/aptlistbugs/logic.rb:93
msgid "Fixed in NMU"
msgstr "Opravené pomocou NMU"

#: ../lib/aptlistbugs/logic.rb:94
msgid "From other Branch"
msgstr "Z inej vetvy"

#: ../lib/aptlistbugs/logic.rb:95
msgid "Resolved in some Version"
msgstr "Vyriešené v niektorej verzi"

#. TRANSLATORS: "W: " is a label for warnings; you may translate it with a suitable abbreviation of the word "warning"
#: ../lib/aptlistbugs/logic.rb:235 ../lib/aptlistbugs/logic.rb:239
#: ../lib/aptlistbugs/logic.rb:277 ../lib/aptlistbugs/logic.rb:293
#: ../lib/aptlistbugs/logic.rb:333 ../lib/aptlistbugs/logic.rb:365
#: ../lib/aptlistbugs/logic.rb:491 ../lib/aptlistbugs/logic.rb:509
#: ../lib/aptlistbugs/logic.rb:611 ../lib/aptlistbugs/logic.rb:821
#: ../lib/aptlistbugs/logic.rb:1028
msgid "W: "
msgstr "W: "

#: ../lib/aptlistbugs/logic.rb:235 ../lib/aptlistbugs/logic.rb:239
msgid "%s IS DEPRECATED. USE --url INSTEAD"
msgstr ""

#: ../lib/aptlistbugs/logic.rb:277 ../lib/aptlistbugs/logic.rb:509
#: ../lib/aptlistbugs/logic.rb:821
msgid "Cannot write to %s"
msgstr "Nie je možné zapisovať do %s"

#: ../lib/aptlistbugs/logic.rb:293
msgid "Unrecognized severity '%s' will be ignored by the Debian BTS."
msgstr "Systém sledovania chýb Debianu bude ignorovať nerozpoznanú závažnosť „%s“."

#: ../lib/aptlistbugs/logic.rb:296
msgid "Bugs of severity %s"
msgstr ""

#: ../lib/aptlistbugs/logic.rb:319
msgid "Unrecognized distribution release '%s'."
msgstr ""

#: ../lib/aptlistbugs/logic.rb:323
msgid "Cannot determine list of distribution releases with command %s"
msgstr ""

#: ../lib/aptlistbugs/logic.rb:333
msgid ""
"sanity check failed: environment variable http_proxy is unset and HTTP_PROXY "
"is set."
msgstr ""
"kontrola zmysluplnosti zlyhala: premenná prostredia http_proxy nie je "
"nastavená a HTTP_PROXY je nastavená."

#: ../lib/aptlistbugs/logic.rb:365
msgid "Cannot execute auto proxy detect command %s"
msgstr ""

#: ../lib/aptlistbugs/logic.rb:384
msgid "You need to specify a command."
msgstr "Musíte zadať príkaz."

#: ../lib/aptlistbugs/logic.rb:394
msgid "Unknown command "
msgstr "Neznámy príkaz "

#: ../lib/aptlistbugs/logic.rb:491 ../libexec/aptcleanup:52
msgid "Cannot read from %s"
msgstr "Nie je možné čítať z %s"

#: ../lib/aptlistbugs/logic.rb:548
msgid "Are you sure you want to install/upgrade the above packages?"
msgstr "Ste si istý, že chcete nainštalovať/aktualizovať horeuvedené balíky?"

#: ../lib/aptlistbugs/logic.rb:600 ../lib/aptlistbugs/logic.rb:625
#: ../lib/aptlistbugs/logic.rb:691
msgid "%s is unknown"
msgstr "%s nie je známy"

#: ../lib/aptlistbugs/logic.rb:611
#, fuzzy
msgid "Failed to invoke querybts."
msgstr "Nepodarilo sa spustiť prehliadač."

#: ../lib/aptlistbugs/logic.rb:616
msgid "You must install the reportbug package to be able to do this"
msgstr "Aby ste to mohli spraviť, musíte nainštalovať balík reportbug"

#: ../lib/aptlistbugs/logic.rb:634
#, fuzzy
msgid "%s is unknown to the BTS"
msgstr "%s nie je známy"

#. TRANSLATORS: "ignored" refers to one singular bug
#: ../lib/aptlistbugs/logic.rb:643 ../lib/aptlistbugs/logic.rb:657
msgid "%s ignored"
msgstr "%s ignorované"

#. TRANSLATORS: "ignored" refers to one singular bug
#: ../lib/aptlistbugs/logic.rb:646
msgid "%s already ignored"
msgstr "%s je už ignorované"

#. TRANSLATORS: %{plist} is a comma-separated list of %{npkgs} packages to be pinned.
#: ../lib/aptlistbugs/logic.rb:668 ../lib/aptlistbugs/logic.rb:747
#, fuzzy
msgid ""
"The following %{npkgs} package will be pinned:\n"
" %{plist}\n"
"Are you sure?"
msgid_plural ""
"The following %{npkgs} packages will be pinned:\n"
" %{plist}\n"
"Are you sure?"
msgstr[0] ""
"Nasledovný %{npkgs} balík bude pripevnený alebo podržaný:\n"
" %{plist}\n"
"Ste si istý?"
msgstr[1] ""
"Nasledovné %{npkgs} balíky budú pripevnené alebo podržané:\n"
" %{plist}\n"
"Ste si istý?"
msgstr[2] ""
"Nasledovných %{npkgs} balíkov bude pripevnených alebo podržaných:\n"
" %{plist}\n"
"Ste si istý?"

#. TRANSLATORS: %{blist} is a comma-separated list of %{nbugs} bugs to be dodged.
#: ../lib/aptlistbugs/logic.rb:706
#, fuzzy
msgid ""
"The following %{nbugs} bug will be dodged:\n"
" %{blist}\n"
"Are you sure?"
msgid_plural ""
"The following %{nbugs} bugs will be dodged:\n"
" %{blist}\n"
"Are you sure?"
msgstr[0] ""
"Nasledovný %{nbugs} balík bude pripevnený alebo podržaný:\n"
" %{blist}\n"
"Ste si istý?"
msgstr[1] ""
"Nasledovné %{nbugs} balíky budú pripevnené alebo podržané:\n"
" %{blist}\n"
"Ste si istý?"
msgstr[2] ""
"Nasledovných %{nbugs} balíkov bude pripevnených alebo podržaných:\n"
" %{blist}\n"
"Ste si istý?"

#: ../lib/aptlistbugs/logic.rb:725
#, fuzzy
msgid "You must install a web browser package to be able to do this"
msgstr "Aby ste to mohli spraviť, musíte nainštalovať balík reportbug"

#: ../lib/aptlistbugs/logic.rb:757
#, fuzzy
msgid "All selected packages are already pinned. Ignoring %s command."
msgstr "Všetky vybrané balík už sú pripevnené alebo podržané. Ignoruje sa príkaz %s."

#: ../lib/aptlistbugs/logic.rb:763
msgid "There are no dodge/pin/ignore operations to undo. Ignoring %s command."
msgstr ""

#: ../lib/aptlistbugs/logic.rb:765
msgid ""
"All the dodge/pin/ignore operations will be undone.\n"
"Are you sure?"
msgstr ""

#. TRANSLATORS: the dashes (-) in the following strings are vertically aligned, please keep their alignment consistent
#: ../lib/aptlistbugs/logic.rb:778
#, fuzzy
msgid ""
"     y     - continue the APT installation.\n"
msgstr ""
"     n     - zastaviť inštaláciu APT.\n"

#: ../lib/aptlistbugs/logic.rb:781
msgid ""
"     n     - stop the APT installation.\n"
msgstr ""
"     n     - zastaviť inštaláciu APT.\n"

#. TRANSLATORS: %{prog} is the name of a program, %{user} is a user name.
#: ../lib/aptlistbugs/logic.rb:785
#, fuzzy
msgid ""
"   <num>   - query the specified bug number\n"
"             (uses %{prog} as user %{user}).\n"
msgstr "číslo chyby"

#: ../lib/aptlistbugs/logic.rb:788
msgid ""
"  #<num>   - same as <num>.\n"
msgstr ""
" #<číslo>  - rovnaké ako <číslo>.\n"

#: ../lib/aptlistbugs/logic.rb:789
msgid ""
"   b<id>   - same as <num>, but query the bug identified by <id>.\n"
msgstr ""
"   b<id>   - rovnaké ako <num>, ale s chybou určenou pomocou <id>.\n"

#: ../lib/aptlistbugs/logic.rb:792
msgid ""
"     r     - redisplay bug lists.\n"
msgstr ""
"     r     - znova zobraziť zoznam chýb.\n"

#: ../lib/aptlistbugs/logic.rb:793
msgid ""
"     c     - compose bug lists in HTML.\n"
msgstr ""

#: ../lib/aptlistbugs/logic.rb:795
msgid ""
"     w     - display bug lists in HTML\n"
"             (uses %{prog} as user %{user}).\n"
msgstr ""

#: ../lib/aptlistbugs/logic.rb:800
#, fuzzy
msgid ""
" d <num>.. - dodge bugs <num> by pinning affected packages\n"
"             (restart APT session to enable).\n"
msgstr "balík"

#: ../lib/aptlistbugs/logic.rb:801
#, fuzzy
msgid ""
" d b<id>.. - dodge bugs identified by <id> by pinning affected packages\n"
"             (restart APT session to enable).\n"
msgstr "balík"

#: ../lib/aptlistbugs/logic.rb:802
#, fuzzy
msgid ""
" p <pkg>.. - pin packages <pkg>\n"
"             (restart APT session to enable).\n"
msgstr ""
" p <bal..> - pripevniť balíky (na zapnutie je potrebné reštartovať APT).\n"

#: ../lib/aptlistbugs/logic.rb:803
#, fuzzy
msgid ""
" p         - pin all the above packages\n"
"             (restart APT session to enable).\n"
msgstr ""
" p         - pripevniť všetky hore uvedené balíky (na zapnutie je potrebné\n"
"             reštartovať APT).\n"

#: ../lib/aptlistbugs/logic.rb:804
msgid ""
" i <num>   - mark bug number <num> as ignored.\n"
msgstr ""
" i <číslo> - označiť chybu č. <číslo> ako ignorovanú.\n"

#: ../lib/aptlistbugs/logic.rb:805
msgid ""
" i b<id>   - mark the bug identified by <id> as ignored.\n"
msgstr ""
" i b<id>   - označiť chybu s identifikátorom <id> ako ignorovanú.\n"

#: ../lib/aptlistbugs/logic.rb:806
msgid ""
" i         - mark all the above bugs as ignored.\n"
msgstr ""

#: ../lib/aptlistbugs/logic.rb:807
msgid ""
"     u     - undo all the dodge/pin/ignore operations done so far.\n"
msgstr ""

#: ../lib/aptlistbugs/logic.rb:808
msgid ""
"     ?     - print this help.\n"
msgstr ""
"     ?     - vypísať tohto pomocníka.\n"

#: ../lib/aptlistbugs/logic.rb:851
msgid ""
"None of the above bugs is assigned to package %s\n"
"Are you sure you want to pin it?"
msgstr ""

#. TRANSLATORS: %{packgl} is a comma-separated list of %{npackg} packages.
#: ../lib/aptlistbugs/logic.rb:877
#, fuzzy
msgid ""
"Restart APT session to enable pinning for the following %{npackg} package:\n"
" %{packgl}\n"
msgid_plural ""
"Restart APT session to enable pinning for the following %{npackg} packages:\n"
" %{packgl}\n"
msgstr[0] "balík"

#. TRANSLATORS: %{sevty} is the severity of some of the bugs found for package %{packg}.
#: ../lib/aptlistbugs/logic.rb:905
msgid "%{sevty} bugs of %{packg} ("
msgstr "%{sevty} chyby balíka %{packg} ("

#. TRANSLATORS: "Found" refers to one singular bug
#: ../lib/aptlistbugs/logic.rb:918
msgid " (Found: %s)"
msgstr " (Nájdených: %s)"

#. TRANSLATORS: "Fixed" refers to one singular bug
#: ../lib/aptlistbugs/logic.rb:920
msgid " (Fixed: %s)"
msgstr " (Opravených: %s)"

#. TRANSLATORS: "Merged" refers to one singular bug
#: ../lib/aptlistbugs/logic.rb:924 ../lib/aptlistbugs/logic.rb:1090
msgid "   Merged with:"
msgstr "   Zlúčených:"

#. TRANSLATORS: %{nbugs} is the number of bugs found for package %{packg}.
#: ../lib/aptlistbugs/logic.rb:939
msgid "%{packg}(%{nbugs} bug)"
msgid_plural "%{packg}(%{nbugs} bugs)"
msgstr[0] "%{packg}(%{nbugs} chyba)"
msgstr[1] "%{packg}(%{nbugs} chyby)"
msgstr[2] "%{packg}(%{nbugs} chýb)"

#: ../lib/aptlistbugs/logic.rb:946
msgid ""
"Summary:\n"
" "
msgstr ""
"Zhrnutie:\n"
" "

#. TRANSLATORS: this is a summary description of the structure of a table (for accessibility)
#: ../lib/aptlistbugs/logic.rb:959
msgid ""
"The top row describes the meaning of the columns; the other rows describe bug "
"reports, one per row"
msgstr ""
"Horný riadok popisuje význam stĺpcov. Ostatné riadky popisujú hlásenia o "
"chybách, jedno na riadok"

#: ../lib/aptlistbugs/logic.rb:964 ../lib/aptlistbugs/logic.rb:1084
msgid "package"
msgstr "balík"

#: ../lib/aptlistbugs/logic.rb:965
msgid "version change"
msgstr "zmena verzie"

#: ../lib/aptlistbugs/logic.rb:966 ../lib/aptlistbugs/logic.rb:1085
msgid "severity"
msgstr "závažnosť"

#: ../lib/aptlistbugs/logic.rb:967 ../lib/aptlistbugs/logic.rb:1083
msgid "bug number"
msgstr "číslo chyby"

#: ../lib/aptlistbugs/logic.rb:968
msgid "description"
msgstr "popis"

#: ../lib/aptlistbugs/logic.rb:986 ../lib/aptlistbugs/logic.rb:989
msgid "Relevant bugs for your upgrade"
msgstr "Chyby relevantné k vašej aktualizácii"

#: ../lib/aptlistbugs/logic.rb:990
msgid "by apt-listbugs"
msgstr "vytvoril apt-listbugs"

#: ../lib/aptlistbugs/logic.rb:1020
msgid ""
"You can view the bug lists in HTML at the following URI:\n"
msgstr ""

#: ../lib/aptlistbugs/logic.rb:1028
msgid "Failed to invoke browser."
msgstr "Nepodarilo sa spustiť prehliadač."

#: ../lib/aptlistbugs/logic.rb:1086
msgid "category of bugs"
msgstr ""

#: ../lib/aptlistbugs/logic.rb:1087
msgid "tags"
msgstr ""

#: ../lib/aptlistbugs/logic.rb:1119 ../lib/aptlistbugs/logic.rb:1207
#: ../lib/aptlistbugs/logic.rb:1416
msgid "Done"
msgstr "Hotovo"

#: ../lib/aptlistbugs/logic.rb:1131
msgid "Not Implemented"
msgstr "Neimplementované"

#. TRANSLATORS: this sentence, followed by the translation of "Done" (see above) should fit in less than 79 columns to work well with default width terminals
#: ../lib/aptlistbugs/logic.rb:1153 ../lib/aptlistbugs/logic.rb:1159
#: ../lib/aptlistbugs/logic.rb:1204 ../lib/aptlistbugs/logic.rb:1207
msgid "Retrieving bug reports..."
msgstr "Získavajú sa hlásenia chýb..."

#: ../lib/aptlistbugs/logic.rb:1162 ../lib/aptlistbugs/logic.rb:1174
#: ../lib/aptlistbugs/logic.rb:1186
msgid " Fail"
msgstr " Zlyhalo"

#: ../lib/aptlistbugs/logic.rb:1164
msgid "HTTP GET failed"
msgstr "HTTP GET zlyhalo"

#: ../lib/aptlistbugs/logic.rb:1166 ../lib/aptlistbugs/logic.rb:1178
#: ../lib/aptlistbugs/logic.rb:1196
msgid "Retry downloading bug information?"
msgstr "Skúsiť znova stiahnuť informácie o chybe?"

#: ../lib/aptlistbugs/logic.rb:1167 ../lib/aptlistbugs/logic.rb:1179
#: ../lib/aptlistbugs/logic.rb:1197
#, fuzzy
msgid "One package at a time?"
msgstr "Po jenom hlásení o chybe?"

#: ../lib/aptlistbugs/logic.rb:1168 ../lib/aptlistbugs/logic.rb:1180
#: ../lib/aptlistbugs/logic.rb:1198
msgid "One bug report at a time?"
msgstr "Po jenom hlásení o chybe?"

#: ../lib/aptlistbugs/logic.rb:1171 ../lib/aptlistbugs/logic.rb:1183
#: ../lib/aptlistbugs/logic.rb:1201
msgid "Exiting with error"
msgstr "Ukončuje sa s chybou"

#: ../lib/aptlistbugs/logic.rb:1171 ../lib/aptlistbugs/logic.rb:1183
#: ../lib/aptlistbugs/logic.rb:1201
msgid "Continue the installation anyway?"
msgstr "Chcete napriek tomu pokračovať v inštalácii?"

#: ../lib/aptlistbugs/logic.rb:1176
msgid "Empty stream from SOAP"
msgstr "Prázdny stream zo SOAP"

#: ../lib/aptlistbugs/logic.rb:1188
msgid "Error retrieving bug reports from the server with the following error message:"
msgstr "Chyba pri získavaní hlásení chýb zo servera s nasledovnou chybovou správou:"

#: ../lib/aptlistbugs/logic.rb:1191
msgid ""
"It appears that your network connection is down. Check network configuration "
"and try again"
msgstr ""
"Zdá sa, že vaše sieťové pripojenie nefunguje. Skontrolujte konfiguráciu siete "
"a skúste to znova"

#: ../lib/aptlistbugs/logic.rb:1193
msgid ""
"It could be because your network is down, or because of broken proxy servers, "
"or the BTS server itself is down. Check network configuration and try again"
msgstr ""
"Mohla to spôsobiť nefunkčnosť vašej siete alebo pokazené proxy servery alebo "
"nedostupnosť samotného servera BTS. Skontrolujte konfiguráciu siete a skúste "
"to znova"

#. TRANSLATORS: this sentence, followed by the translation of "Done" (see above) should fit in less than 79 columns to work well with default width terminals
#: ../lib/aptlistbugs/logic.rb:1382 ../lib/aptlistbugs/logic.rb:1402
#: ../lib/aptlistbugs/logic.rb:1415 ../lib/aptlistbugs/logic.rb:1416
msgid "Parsing Found/Fixed information..."
msgstr "Spracúvajú sa informácie: nájdené/opravené..."

#: ../libexec/aptcleanup:123
msgid " Fixed packages: "
msgstr " Opravené balíky: "

#~ msgid ""
#~ " -H <hostname>    : Hostname of Debian Bug Tracking System [%s].\n"
#~ msgstr ""
#~ " -H <server>      : Doménový názov servera, kde beží Systém na sledovanie\n"
#~ "                    chýb Debianu (BTS) [%s].\n"
#~ msgid ""
#~ " -p <port>        : Port number of the server [%s].\n"
#~ msgstr ""
#~ " -p <port>        : Číslo portu servera [%s].\n"
#~ msgid ""
#~ " list <pkg...>    : List bug reports of the specified packages.\n"
#~ msgstr ""
#~ " list <bal...>    : Vypísať hlásenia chýb určených balíkov.\n"
#~ msgid ""
#~ " rss <pkg...>     : List bug reports of the specified packages in RSS.\n"
#~ msgstr ""
#~ " rss <bal...>     : Vypísať hlásenia chýb určených balíkov v RSS.\n"
#~ msgid ""
#~ " p <pkg>.. - pin pkgs\n"
#~ "             (restart APT session to enable).\n"
#~ msgstr ""
#~ " p <bal..> - pripevniť balíky (na zapnutie je potrebné reštartovať APT).\n"
#~ msgid ""
#~ " p         - pin all the above pkgs\n"
#~ "             (restart APT session to enable).\n"
#~ msgstr ""
#~ " p         - pripevniť všetky hore uvedené balíky (na zapnutie je potrebné\n"
#~ "             reštartovať APT).\n"
#~ msgid ""
#~ "APT Pre-Install-Pkgs is not giving me expected 'VERSION 3' string.\n"
#~ msgstr ""
#~ "APT Pre-Install-Pkgs nedodáva očakávaný reťazec „VERSION 3“.\n"
#~ msgid ""
#~ "APT Pre-Install-Pkgs is giving me fewer fields than expected.\n"
#~ msgstr ""
#~ "APT Pre-Install-Pkgs dodáva menej polí ako sa očakávalo.\n"
#~ msgid ""
#~ "APT Pre-Install-Pkgs is giving me an invalid direction of version change.\n"
#~ msgstr ""
#~ "APT Pre-Install-Pkgs udáva neplatný smer zmeny verzie.\n"
#~ msgid "********** on_hold IS DEPRECATED. USE p INSTEAD to use pin **********"
#~ msgstr ""
#~ "* POUŽITIE on_hold SA ZAVRHUJE. ak chcete použiť pin, POUŽITE NAMIESTO TOHO p "
#~ "*"
#~ msgid ""
#~ "The following %{npkgs} package will be pinned or on hold:\n"
#~ " %{plist}\n"
#~ "Are you sure?"
#~ msgid_plural ""
#~ "The following %{npkgs} packages will be pinned or on hold:\n"
#~ " %{plist}\n"
#~ "Are you sure?"
#~ msgstr[0] ""
#~ "Nasledovný %{npkgs} balík bude pripevnený alebo podržaný:\n"
#~ " %{plist}\n"
#~ "Ste si istý?"
#~ msgstr[1] ""
#~ "Nasledovné %{npkgs} balíky budú pripevnené alebo podržané:\n"
#~ " %{plist}\n"
#~ "Ste si istý?"
#~ msgstr[2] ""
#~ "Nasledovných %{npkgs} balíkov bude pripevnených alebo podržaných:\n"
#~ " %{plist}\n"
#~ "Ste si istý?"
#~ msgid "All selected packages are already pinned or on hold. Ignoring %s command."
#~ msgstr "Všetky vybrané balík už sú pripevnené alebo podržané. Ignoruje sa príkaz %s."
#~ msgid ""
#~ "     y     - continue the APT installation, but do not mark the bugs\n"
#~ "             as ignored.\n"
#~ msgstr ""
#~ "     y     - pokračovať v inštalácii APT, ale neoznačovať chyby ako\n"
#~ "             ignorované.\n"
#~ msgid ""
#~ "     a     - continue the APT installation and mark all the above bugs\n"
#~ "             as ignored.\n"
#~ msgstr ""
#~ "     a     - pokračovať v inštalácii APT, a označiť všetky hore uvedené\n"
#~ "             chyby ako ignorované.\n"
#~ msgid ""
#~ " p <pkg..> - pin pkgs (restart APT session to enable).\n"
#~ msgstr ""
#~ " p <bal..> - pripevniť balíky (na zapnutie je potrebné reštartovať APT).\n"
#~ msgid ""
#~ " p         - pin all the above pkgs (restart APT session to enable).\n"
#~ msgstr ""
#~ " p         - pripevniť všetky hore uvedené balíky (na zapnutie je potrebné\n"
#~ "             reštartovať APT).\n"
#~ msgid ""
#~ "%{packgl} pinned by adding Pin preferences in %{filenm}. Restart APT session "
#~ "to enable"
#~ msgstr ""
#~ "%{packgl} pripevnené pridaním volieb Pin do %{filenm}. Zapnete reštartovaním "
#~ "relácie APT"
#~ msgid "%s held. Restart APT session to enable"
#~ msgstr "%s podržaný. Zapnete reštartovaním relácie APT"
#~ msgid ""
#~ " -y               : Assume that you select yes for all questions.\n"
#~ msgstr ""
#~ " -y               : Predpokladať odpoveď áno na všetky otázky.\n"
#~ msgid ""
#~ " -n               : Assume that you select no for all questions.\n"
#~ msgstr ""
#~ " -n               : Predpokladať odpoveď nie na všetky otázky.\n"
#~ msgid ""
#~ "   <num>   - query the specified bug number (requires reportbug).\n"
#~ msgstr ""
#~ "  <číslo>  - stiahnuť požadované číslo chybového hlásenia (vyžaduje "
#~ "reportbug).\n"
#~ msgid ""
#~ "     w     - display bug lists in HTML (uses %s).\n"
#~ msgstr ""
#~ "     w     - zobraziť zoznamy chýb v HTML (používa %s).\n"
#~ msgid ""
#~ " -C <apt.conf>    : apt.conf file to use.\n"
#~ msgstr ""
#~ " -C <apt.conf>    : Ktorý súbor apt.conf použiť.\n"
#~ msgid "Bug reports which are marked as %s in the bug tracking system"
#~ msgstr "Hlásenia chýb označené ako %s v systéme na sledovanie chýb"
#~ msgid "Bug reports"
#~ msgstr "Hlásenia chýb"
#~ msgid "Package upgrade information in question"
#~ msgstr "Súvisiaca informácia o aktualizácii balíka"
#~ msgid "unfixed"
#~ msgstr "neopravené"
#~ msgid "tagged as pending a fix"
#~ msgstr "označené ako oprava čakajúca na nahranie"
#~ msgid "W: sanity check failed: environment variable http_proxy is set and soap_use_proxy is not 'on'."
#~ msgstr "W: kontrola zmysluplnosti zlyhala: premenná prostredia http_proxy je nastavená a HTTP_PROXY nie je nastavená."
#~ msgid "%s(%d bug)"
#~ msgid_plural "%s(%d bugs)"
#~ msgstr[0] "%s(%s chyba)"
#~ msgstr[1] "%s(%s chýb)"
#~ msgstr[2] "%s(%s chýb)"
#~ msgid "Reading package fields..."
#~ msgstr "Čítajú sa polia balíka..."
#~ msgid "Reading package status..."
#~ msgstr "Čítajú sa stav balíka..."
#~ msgid "in the bug tracking system</caption>"
#~ msgstr "v systéme na sledovanie chýb</caption>"
#~ msgid "%s(%s bugs)"
#~ msgstr "%s(%s chýb)"
#~ msgid " -f               : Retrieve bug reports from BTS forcibly.\n"
#~ msgstr " -f               : násilne získať hlásenia chýb z BTS.\n"
